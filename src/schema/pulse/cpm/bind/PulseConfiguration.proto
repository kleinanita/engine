syntax = "proto3";
package pulse.cpm.bind;
option java_package = "com.kitware.pulse.cpm.bind";
option csharp_namespace = "pulse.cpm.bind";
option optimize_for = SPEED;

import "pulse/cdm/bind/Actions.proto";
import "pulse/cdm/bind/Enums.proto";
import "pulse/cdm/bind/Properties.proto";
import "pulse/cdm/bind/Engine.proto";
import "pulse/cdm/bind/Environment.proto";
import "pulse/cdm/bind/PatientNutrition.proto";
import "pulse/cdm/bind/ElectroCardioGram.proto";

message ConfigurationData

{
  message BloodChemistryConfigurationData
  {
    pulse.cdm.bind.ScalarMassPerAmountData             MeanCorpuscularHemoglobin                             = 1;
    pulse.cdm.bind.ScalarVolumeData                    MeanCorpuscularVolume                                 = 2;
    pulse.cdm.bind.ScalarLengthData                    StandardDiffusionDistance                             = 3;
    pulse.cdm.bind.ScalarAreaPerTimePressureData       StandardOxygenDiffusionCoefficient                    = 4;
  }
  
  message CardiovascularConfigurationData
  {
    pulse.cdm.bind.ScalarPressurePerVolumeData         LeftHeartElastanceMaximum                             = 1;
    pulse.cdm.bind.ScalarPressurePerVolumeData         LeftHeartElastanceMinimum                             = 2;
    pulse.cdm.bind.Scalar0To1Data                      MinimumBloodVolumeFraction                            = 3;
    pulse.cdm.bind.ScalarPressurePerVolumeData         RightHeartElastanceMaximum                            = 4;
    pulse.cdm.bind.ScalarPressurePerVolumeData         RightHeartElastanceMinimum                            = 5;
    pulse.cdm.bind.ScalarData                          StandardPulmonaryCapillaryCoverage                    = 6;
  }
  
  message CircuitConfigurationData
  {
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     CardiovascularOpenResistance                         = 1;;
    pulse.cdm.bind.ScalarElectricResistanceData        DefaultClosedElectricResistance                      = 2;;
    pulse.cdm.bind.ScalarElectricResistanceData        DefaultOpenElectricResistance                        = 3;;
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     DefaultClosedFlowResistance                          = 4;;
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     DefaultOpenFlowResistance                            = 5;;
    pulse.cdm.bind.ScalarHeatResistanceData            DefaultClosedHeatResistance                          = 6;;
    pulse.cdm.bind.ScalarHeatResistanceData            DefaultOpenHeatResistance                            = 7;;
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     MachineClosedResistance                              = 8;;
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     MachineOpenResistance                                = 9;;
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     RespiratoryClosedResistance                          = 10;
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     RespiratoryOpenResistance                            = 11;
  }
  
  message ConstantsConfigurationData
  {
    pulse.cdm.bind.ScalarData                                   OxygenMetabolicConstant                     = 1;
    pulse.cdm.bind.ScalarPowerPerAreaTemperatureToTheFourthData StefanBoltzmann                             = 2;
    pulse.cdm.bind.ScalarHeatCapacitancePerAmountData           UniversalGasConstant                        = 3;
  }
  
  message DrugsConfigurationData
  {
    pulse.cdm.bind.eSwitch                             PDModel                                              = 1;
  }
  
  message ECGConfigurationData
  {
    oneof ElectroCardioGramInterpolator
    {
      string                                           InterpolatorFileName                                 = 1;/**<< @brief */
      pulse.cdm.bind.ElectroCardioGramWaveformListData Interpolator                                         = 2;/**<< @brief */
    }
  }
  
  message EnergyConfigurationData
  {
    pulse.cdm.bind.ScalarHeatCapacitancePerMassData    BodySpecificHeat                                     = 1;
    pulse.cdm.bind.ScalarData                          CarbonDioxideProductionFromOxygenConsumptionConstant = 2;
    pulse.cdm.bind.ScalarTemperatureData               CoreTemperatureLow                                   = 3;
    pulse.cdm.bind.ScalarTemperatureData               CoreTemperatureHigh                                  = 4;
    pulse.cdm.bind.ScalarTemperatureData               DeltaCoreTemperatureLow                              = 5;
    pulse.cdm.bind.ScalarEnergyPerAmountData           EnergyPerATP                                         = 6;
    pulse.cdm.bind.ScalarHeatConductanceData           SweatHeatTransfer                                    = 7;
    pulse.cdm.bind.ScalarEnergyPerMassData             VaporizationEnergy                                   = 8;
    pulse.cdm.bind.ScalarHeatCapacitancePerMassData    VaporSpecificHeat                                    = 9;
  }
  
  message EnvironmentConfigurationData
  {
    oneof Option
    {
      pulse.cdm.bind.EnvironmentalConditionsData       InitialConditions                                    = 1;/**<< @brief An environment conditions object with properties to set in the system environmental conditions. */
      string                                           InitialConditionsFile                                = 2;/**<< @brief File containing an environment conditions objet with properties to set in the environmental conditions. */
    }
    pulse.cdm.bind.ScalarMassPerVolumeData             AirDensity                                           = 3;
    pulse.cdm.bind.ScalarHeatCapacitancePerMassData    AirSpecificHeat                                      = 4;
    pulse.cdm.bind.ScalarMassPerAmountData             MolarMassOfDryAir                                    = 5;
    pulse.cdm.bind.ScalarMassPerAmountData             MolarMassOfWaterVapor                                = 6;
    pulse.cdm.bind.ScalarMassPerVolumeData             WaterDensity                                         = 7;
  }
  
  message GastrointestinalConfigurationData
  {
    oneof Option
    {
      pulse.cdm.bind.NutritionData                     InitialStomachContents                               = 1;/**<< @brief An stomach contents object with properties to set in the system. */
      string                                           InitialStomachContentsFile                           = 2;/**<< @brief File containing an stomach contents objet with properties to set in the. */
    }
    pulse.cdm.bind.Scalar0To1Data                      CalciumAbsorptionFraction                            = 3;
    pulse.cdm.bind.ScalarMassPerTimeData               CalciumDigestionRate                                 = 4;
    pulse.cdm.bind.Scalar0To1Data                      CarbohydrateAbsorptionFraction                       = 5;
    pulse.cdm.bind.ScalarMassPerTimeData               DefaultCarbohydrateDigestionRate                     = 6;
    pulse.cdm.bind.ScalarMassPerTimeData               DefaultFatDigestionRate                              = 7;
    pulse.cdm.bind.ScalarMassPerTimeData               DefaultProteinDigestionRate                          = 8;
    pulse.cdm.bind.Scalar0To1Data                      FatAbsorptionFraction                                = 9;
    pulse.cdm.bind.Scalar0To1Data                      ProteinToUreaFraction                                = 10;
    pulse.cdm.bind.ScalarVolumePerTimeData             WaterDigestionRate                                   = 11;
  }
  
  message NervousConfigurationData
  {
    pulse.cdm.bind.eSwitch                             EnableCerebrospinalFluid                             = 1;
    pulse.cdm.bind.eSwitch                             ChemoreceptorFeedback                                = 2;
    pulse.cdm.bind.eSwitch                             BaroreceptorFeedback                                 = 3;
    pulse.cdm.bind.ScalarTimeData                      HeartElastanceDistributedTimeDelay                   = 4;
    pulse.cdm.bind.ScalarTimeData                      HeartRateDistributedTimeDelay                        = 5;
    pulse.cdm.bind.ScalarData                          NormalizedHeartRateIntercept                         = 6;
    pulse.cdm.bind.ScalarData                          NormalizedHeartRateSympatheticSlope                  = 7;
    pulse.cdm.bind.ScalarData                          NormalizedHeartRateParasympatheticSlope              = 8;
    pulse.cdm.bind.ScalarData                          NormalizedHeartElastanceIntercept                    = 9;
    pulse.cdm.bind.ScalarData                          NormalizedHeartElastanceSympatheticSlope             = 10;
    pulse.cdm.bind.ScalarData                          NormalizedResistanceIntercept                        = 11;
    pulse.cdm.bind.ScalarData                          NormalizedResistanceSympatheticSlope                 = 12;
    pulse.cdm.bind.ScalarData                          NormalizedComplianceIntercept                        = 13;
    pulse.cdm.bind.ScalarData                          NormalizedComplianceParasympatheticSlope             = 14;
    pulse.cdm.bind.ScalarLengthData                    PupilDiameterBaseline                                = 15;
    pulse.cdm.bind.ScalarData                          ResponseSlope                                        = 16;
    pulse.cdm.bind.ScalarTimeData                      SystemicResistanceDistributedTimeDelay               = 17;
    pulse.cdm.bind.ScalarTimeData                      VenousComplianceDistributedTimeDelay                 = 18;
  }
  
  message RenalConfigurationData
  {
    pulse.cdm.bind.eSwitch                             EnableRenal                                          = 1;
                                                       
    pulse.cdm.bind.ScalarMassPerVolumeData             PlasmaSodiumConcentrationSetPoint                    = 2;
    pulse.cdm.bind.ScalarMassPerVolumeData             PeritubularPotassiumConcentrationSetPoint            = 3;
                                                       
    pulse.cdm.bind.ScalarAreaData                      LeftGlomerularFilteringSurfaceAreaBaseline           = 4;
    pulse.cdm.bind.ScalarVolumePerTimePressureAreaData LeftGlomerularFluidPermeabilityBaseline              = 5;
    pulse.cdm.bind.ScalarAreaData                      LeftTubularReabsorptionFilteringSurfaceAreaBaseline  = 6;
    pulse.cdm.bind.ScalarVolumePerTimePressureAreaData LeftTubularReabsorptionFluidPermeabilityBaseline     = 7;
                                                       
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     MaximumAfferentResistance                            = 8;
    pulse.cdm.bind.ScalarPressureTimePerVolumeData     MinimumAfferentResistance                            = 9;
                                                       
    pulse.cdm.bind.ScalarAreaData                      RightGlomerularFilteringSurfaceAreaBaseline          = 10;
    pulse.cdm.bind.ScalarVolumePerTimePressureAreaData RightGlomerularFluidPermeabilityBaseline             = 11;
    pulse.cdm.bind.ScalarAreaData                      RightTubularReabsorptionFilteringSurfaceAreaBaseline = 12;
    pulse.cdm.bind.ScalarVolumePerTimePressureAreaData RightTubularReabsorptionFluidPermeabilityBaseline    = 13;
                                                       
    pulse.cdm.bind.ScalarMassPerTimeData               TargetSodiumDelivery                                 = 14;
  }
  
  message RespiratoryConfigurationData
  {
    pulse.cdm.bind.ScalarPressureData                  CentralControllerCO2PressureSetPoint                 = 1;
    pulse.cdm.bind.ScalarData                          CentralVentilatoryControllerGain                     = 2;
    pulse.cdm.bind.ScalarVolumeData                    MinimumAllowableTidalVolume                          = 3;
    pulse.cdm.bind.ScalarTimeData                      MinimumAllowableInspiratoryAndExpiratoryPeriod       = 4;
    pulse.cdm.bind.ScalarPressureData                  PeripheralControllerCO2PressureSetPoint              = 5;
    pulse.cdm.bind.ScalarData                          PeripheralVentilatoryControllerGain                  = 6;
    pulse.cdm.bind.ScalarVolumePerTimeData             PulmonaryVentilationRateMaximum                      = 7;
    pulse.cdm.bind.ScalarVolumeData                    VentilationTidalVolumeIntercept                      = 8;
    pulse.cdm.bind.ScalarPressureData                  VentilatoryOcclusionPressure                         = 9;
  }
  
  message TissueConfigurationData
  {
    pulse.cdm.bind.eSwitch                             EnableTissue                                         = 1;
  }
  
  pulse.cdm.bind.ScalarTimeData                        TimeStep                                             = 1;/**<< @brief */
  pulse.cdm.bind.eSwitch                               AllowDynamicTimeStep                                 = 2;/**<< @brief The time step will be replaced to whatever is provided via AdvanceModelTime(double time, const TimeUnit& unit)*/
  
  oneof StabilizationCriteria
  {
    string                                             StabilizationFileName                                = 3;/**<< @brief */
    pulse.cdm.bind.TimedStabilizationData              TimedStabilization                                   = 4;/**<< @brief */
    pulse.cdm.bind.DynamicStabilizationData            DynamicStabilization                                 = 5;/**<< @brief */
  }
  pulse.cdm.bind.eSwitch                               WritePatientBaselineFile                             = 6;/**<< @brief Inform execution to write out patient object after stabilization completes. */
  BloodChemistryConfigurationData                      BloodChemistryConfiguration                          = 7;
  CardiovascularConfigurationData                      CardiovascularConfiguration                          = 8;
  CircuitConfigurationData                             CircuitConfiguration                                 = 9;
  ConstantsConfigurationData                           ConstantsConfiguration                               = 10;
  DrugsConfigurationData                               DrugsConfiguration                                   = 11;
  ECGConfigurationData                                 ECGConfiguration                                     = 12;
  EnergyConfigurationData                              EnergyConfiguration                                  = 13;
  EnvironmentConfigurationData                         EnvironmentConfiguration                             = 14;
  GastrointestinalConfigurationData                    GastrointestinalConfiguration                        = 15;
  NervousConfigurationData                             NervousConfiguration                                 = 16;
  RenalConfigurationData                               RenalConfiguration                                   = 17;
  RespiratoryConfigurationData                         RespiratoryConfiguration                             = 18;
  TissueConfigurationData                              TissueConfiguration                                  = 19;
  pulse.cdm.bind.OverridesData                         InitialOverrides                                     = 20;
}
 