# Syntax
# Each group tag indicates the beginning of a new verification set which will be summarized and sent out in its own email.  
# Notice how information from the header can be specified again under a group tag to override the defaults.

# DASH = Don't Run
# A line that begins with a dash tells the verifier not to run the scenario, but to still include its results in the report.  
# This allows us to run and generate a quick basic report and include these results in later emails without re-running the scenarios.

# ASTERISK = Expected Failure (Run Yellow)
# A line that begins with an asterisk tells the verifier that failure is to be expected from the scenario.  
# These scenarios will not be run and will be placed in the separate yellow block of the email.

ReportName=Scenario Verification Test Summary
ExecuteTests=true
PlotResults=true
PercentDifference=2.0
Threads=-1

# These are used for testing and utilizing serialization
# Replace patient file with it's associated patient state, if scenario has no conditions
#UseStates=true
# AutoSerialize = Directory,Period(in seconds, 0 turns off constant saving),AddTimeStamps,AfterActions,Reload
#AutoSerialization=./states/testing/,0,On,On,On,json

Executor=com.kitware.pulse.engine.testing.ScenarioTestDriver

Macro ScenarioTest=ScenarioTestDriver FastPlot Baseline=scenarios/ Computed=./test_results/scenarios

@group

patient/BasicStandard.json = ScenarioTest

@group Patient
patient/AirwayObstructionVaried.json = ScenarioTest
patient/AcuteStress.json = ScenarioTest
patient/ARDSExacerbation.json = ScenarioTest
patient/ARDSModerateBothLungs.json = ScenarioTest
patient/AsthmaAttackModerateAcute.json = ScenarioTest
patient/AsthmaAttackSevereAcute.json = ScenarioTest
patient/AsthmaAttackLifeThreateningAcute.json = ScenarioTest
*patient/Anemia30.json = ScenarioTest
patient/Baroreceptors.json = ScenarioTest
patient/BrainInjury.json = ScenarioTest
patient/BronchoConstrictionVaried.json = ScenarioTest
patient/COPDExacerbation.json = ScenarioTest
patient/COPDSevereEmphysema.json = ScenarioTest
patient/COPDSevereBronchitis.json = ScenarioTest
patient/Cough.json = ScenarioTest
patient/CPRForceScaleMax.json = ScenarioTest
patient/CPRForceScale.json = ScenarioTest
patient/CPRForce.json = ScenarioTest
patient/DyspneaVaried.json = ScenarioTest
patient/EffusionCondition.json = ScenarioTest
patient/EffusionConditionPlus.json = ScenarioTest
patient/HemorrhageClass1Femoral.json = ScenarioTest
patient/HemorrhageClass2Blood.json = ScenarioTest
patient/HemorrhageClass2BrachialArtery.json = ScenarioTest
patient/HemorrhageClass2InternalMultiple.json = ScenarioTest
patient/HemorrhageClass2InternalSpleen.json = ScenarioTest
patient/HemorrhageClass2NoFluid.json = ScenarioTest
patient/HemorrhageClass2Saline.json = ScenarioTest
patient/HemorrhageClass3NoFluid.json = ScenarioTest
patient/HemorrhageClass3PackedRBC.json = ScenarioTest
patient/HemorrhageClass4NoFluid.json = ScenarioTest
patient/HemorrhageGroup1.json = ScenarioTest
patient/HemorrhageGroup2.json = ScenarioTest
patient/HemorrhageGroup3.json = ScenarioTest
patient/HemorrhageGroup4.json = ScenarioTest
patient/HemorrhageGroup5.json = ScenarioTest
patient/HemorrhageGroup6.json = ScenarioTest
patient/HemorrhageInternalSeverity.json = ScenarioTest
patient/HemorrhageSeverity1.json = ScenarioTest
patient/HemorrhageSeverityMultipleCompartments.json = ScenarioTest
patient/HemorrhageSeverityToFlowToSeverity.json = ScenarioTest
patient/HemorrhageToShock.json = ScenarioTest
patient/HemorrhageVaryingSeverity.json = ScenarioTest
patient/LobarPneumoniaExacerbation.json = ScenarioTest
patient/LobarPneumoniaModerateBothLungs.json = ScenarioTest
patient/LobarPneumoniaSevereLeftLobe.json = ScenarioTest
patient/LobarPneumoniaSevereRightLung.json = ScenarioTest
patient/PulmonaryFibrosisSevere.json = ScenarioTest
patient/RenalStenosisSevereBilateral.json = ScenarioTest
patient/RenalStenosisModerateUnilateral.json = ScenarioTest
patient/RespiratoryFatigue.json = ScenarioTest
patient/SinusBradycardia.json = ScenarioTest
patient/SinusTachycardia.json = ScenarioTest
patient/TensionPneumothoraxBilateral.json = ScenarioTest
patient/TensionPneumothoraxClosedVaried.json = ScenarioTest
patient/TensionPneumothoraxOpenVaried.json = ScenarioTest
*patient/VentricularSystolicDysfunction.json = ScenarioTest
patient/ZeroAnemia.json = ScenarioTest
patient/ZeroARDS.json = ScenarioTest
patient/ZeroCOPD.json = ScenarioTest
patient/ZeroLobarPneumonia.json = ScenarioTest
patient/ZeroPericardialEffusion.json = ScenarioTest
patient/ZeroPulmonaryFibrosis.json = ScenarioTest
patient/ZeroRenalStenosis.json = ScenarioTest

@group EnergyEnvironment
energyenvironment/AlveolarArterialGradientEnvironments.json = ScenarioTest
energyenvironment/CarbonMonoxideExtreme.json = ScenarioTest
energyenvironment/CarbonMonoxideThreshold.json = ScenarioTest
#energyEnvironment/CarbonMonoxideThresholdLong.json = ScenarioTest
energyenvironment/ColdWaterSubmersion.json = ScenarioTest
energyenvironment/ExerciseStages.json = ScenarioTest
energyenvironment/ExerciseVO2max.json = ScenarioTest
energyenvironment/FireFighter.json = ScenarioTest
energyenvironment/HighAltitudeEnvironmentChange.json = ScenarioTest
energyenvironment/HighAltitudeEnvironmentFileChange.json = ScenarioTest
energyenvironment/InitialHighAltitudeEnvironmentFile.json = ScenarioTest
energyenvironment/InitialHighAltitudeEnvironmentState.json = ScenarioTest
energyenvironment/ThermalApplication.json = ScenarioTest

@group Equipment
equipment/AnesthesiaMachineVariedConfiguration.json = ScenarioTest
equipment/AnesthesiaMachineEndotrachealTubeLeakVaried.json = ScenarioTest
equipment/AnesthesiaMachineExpiratoryValveObstructionVaried.json = ScenarioTest
equipment/AnesthesiaMachineExpiratoryValveLeakVaried.json = ScenarioTest
equipment/AnesthesiaMachineInspiratoryValveObstructionVaried.json = ScenarioTest
equipment/AnesthesiaMachineInspiratoryValveLeakVaried.json = ScenarioTest
equipment/AnesthesiaMachineMaskLeakVaried.json = ScenarioTest
equipment/AnesthesiaMachineOxygenTankPressureLoss.json = ScenarioTest
equipment/AnesthesiaMachineOxygenWallPressureLoss.json = ScenarioTest
equipment/AnesthesiaMachineSodaLimeFailureVaried.json = ScenarioTest
equipment/AnesthesiaMachineVaporizerFailureVaried.json = ScenarioTest
equipment/AnesthesiaMachineVentilatorPressureLossVaried.json = ScenarioTest
equipment/AnesthesiaMachineYpieceDisconnectVaried.json = ScenarioTest
equipment/PositivePressureVentilation.json = ScenarioTest
equipment/EsophagealIntubation.json = ScenarioTest
equipment/InhalerOneActuation.json = ScenarioTest
equipment/InhalerOneActuationWithSpacer.json = ScenarioTest
equipment/InhalerOneActuationIncorrectUse.json = ScenarioTest
equipment/InhalerOneActuationWithSpacerIncorrectUse.json = ScenarioTest
equipment/InhalerTwoActuations.json = ScenarioTest
equipment/MainstemIntubation.json = ScenarioTest
equipment/MechanicalVentilation.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_ARDS_Varied.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_COPD_Varied.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Healthy.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Mild_ARDS.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Mild_COPD.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Moderate_ARDS.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Moderate_COPD.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Severe_ARDS.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Severe_COPD.json = ScenarioTest
equipment/MechanicalVentilator_PC-CMV_Varied.json = ScenarioTest
equipment/MechanicalVentilator_VC-AC_ARDS.json = ScenarioTest
equipment/MechanicalVentilator_VC-AC_Healthy.json = ScenarioTest
equipment/MechanicalVentilator_VC-CMV_Healthy.json = ScenarioTest
equipment/NasalCannula.json = ScenarioTest
equipment/NonRebreatherMask.json = ScenarioTest
equipment/SimpleMask.json = ScenarioTest

@group Nutrition
nutrition/Nutrition.json = ScenarioTest
nutrition/SodiumIngestion.json = ScenarioTest
nutrition/WaterIngestion.json = ScenarioTest

@group Drugs
drug/Albuterol.json = ScenarioTest
drug/Desflurane.json = ScenarioTest
drug/Epinephrine.json = ScenarioTest
drug/Fentanyl.json = ScenarioTest
drug/Furosemide.json = ScenarioTest
drug/Ketamine.json = ScenarioTest
drug/Midazolam.json = ScenarioTest
drug/Morphine.json = ScenarioTest
drug/Naloxone.json = ScenarioTest
drug/Norepinephrine.json = ScenarioTest
drug/NorepinephrineBolus.json = ScenarioTest
drug/Pralidoxime.json = ScenarioTest
drug/Prednisone.json = ScenarioTest
drug/Propofol.json = ScenarioTest
drug/Rocuronium.json = ScenarioTest
drug/Succinylcholine.json = ScenarioTest

@group Combined
combined/Gus.json = ScenarioTest
combined/Joel.json = ScenarioTest
combined/Hassan.json = ScenarioTest
combined/Cynthia.json = ScenarioTest
combined/Nathan.json = ScenarioTest

@group Showcase
showcase/CombatMultitrauma.json = ScenarioTest
showcase/AsthmaAttack.json = ScenarioTest
showcase/EnvironmentExposure.json = ScenarioTest
showcase/HeatStroke.json = ScenarioTest
showcase/COVID19Ventilation.json = ScenarioTest

@group Miscellaneous
miscellaneous/AirwayObstructionDeath.json = ScenarioTest
miscellaneous/AsthmaAttackDeath.json = ScenarioTest
miscellaneous/ComplianceCurve.json = ScenarioTest
miscellaneous/Comprehensive.json = ScenarioTest
miscellaneous/ConsciousRespiration.json = ScenarioTest
miscellaneous/HouseFireSmoke.json = ScenarioTest
miscellaneous/ImpairedAlveolarExchangeFraction.json = ScenarioTest
miscellaneous/ImpairedAlveolarExchangeSeverity.json = ScenarioTest
miscellaneous/ImpairedAlveolarExchangeSurfaceArea.json = ScenarioTest
miscellaneous/MultiDrug.json = ScenarioTest
miscellaneous/PulmonaryShunt.json = ScenarioTest
miscellaneous/SpirometryComparison.json = ScenarioTest
miscellaneous/SpirometryHealthy.json = ScenarioTest
miscellaneous/SpirometryPulmonaryFibrosis.json = ScenarioTest
miscellaneous/TBIandDrugs.json = ScenarioTest
miscellaneous/ZeroImpairedAlveolarExchange.json = ScenarioTest
miscellaneous/ZeroPulmonaryShunt.json = ScenarioTest


@group Compartments
#compartments/UserSpecific.json = ScenarioTest
compartments/AnesthesiaMachineCompartments.json = ScenarioTest
compartments/ChymeCompartments.json = ScenarioTest
compartments/LymphCompartments.json = ScenarioTest
compartments/PulmonaryCompartments.json = ScenarioTest
compartments/TemperatureCompartments.json = ScenarioTest
compartments/TissueCompartments.json = ScenarioTest
compartments/UrineCompartments.json = ScenarioTest
compartments/VascularCompartments.json = ScenarioTest
